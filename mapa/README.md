Este é um mapa criado através do TiddlyMap, um plugin do TiddlyWiki. Seguem algumas instruções:

Os comandos a seguir rodam no GNU/Linux:
* Sugestão: baixe e instale uma Máquina Virtual, com os pré-requisitos instalados em:
	* http://www.inf.ufg.br/~marceloakira/vms/ 

1. Instalação do Tiddlywiki:
	1. Instale o nodejs: ... todo ...
	2. Instale o npm: ... todo ...
	3. Instalar o TiddlyWiki: npm install -g tiddywiki

2. Copiar o mapa:
	1. Instale o git: ... todo ...
	2. Clone este projeto: git clone https://gitlab.com/sas-cc-2019-1/meta.git

3. Abra o mapa:
	1. Entre na pasta clonada: cd meta
	2. Rode o comando: tiddlywiki ./mapa --server 8201 $:/core/save/all text/plain text/html   0.0.0.0

